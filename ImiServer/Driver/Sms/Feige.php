<?php

namespace ImiApp\ImiServer\Driver\Sms;

use Yurun\Util\HttpRequest;

/**
 * 飞鸽传书短信
 * @author Saopig <1306222220@qq.com>
 */
class Feige
{
    /**
     * 错误信息
     * @var $_error
     */
    private $_error;

    /**
     * 接口地址
     * @var string
     */
    private $api_url = 'https://api.4321.sh/sms/';

    /**
     * 发送单条短信
     * @param $mobile
     * @param $template
     * @param $code
     * @return bool
     */
    public function send($mobile, $template, $code, int $expire = 1800): bool
    {
        $url = $this->api_url . 'template';
        $client = new HttpRequest;
        $postData = [
            'apikey'        => (string) config('sms_id'),
            'secret'        => (string) config('sms_key'),
            'sign_id'       => (int) config("sms_token"),
            'templateid'    => $template,
            'content'       => (string) $code,
            'mobile'        => (string) $mobile,
        ];
        $response = $client->headers([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json;charset=utf-8',
        ])->post($url, json_encode($postData, 256 | 64));
        $data = (string)$response->getBody();
        $json = json_decode($data, true);
        if (isset($json['code']) && $json['code'] == '0') {
            return true;
        } else {
            $this->setError($json['msg'] ?? '未知错误');
            return false;
        }
    }

    public function getError()
    {
        return $this->_error;
    }

    private function setError($error)
    {
        $this->_error = $error;
    }
}