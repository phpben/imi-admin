<?php

namespace ImiApp\ImiServer\Driver\Sms;

use Yurun\Util\HttpRequest;

/**
 * 云片短信
 * @author Saopig <ftlh2005@gmail.com>
 */
class Yunpian
{
    /**
     * 错误信息
     * @var $_error
     */
    private $_error;

    /**
     * 接口地址
     * @var string
     */
    private $api_url = 'https://sms.yunpian.com/v2/sms/';

    /**
     * 发送单条短信
     * @param $mobile
     * @param $template
     * @param $code
     * @param int $expire
     * @return bool
     */
    public function send($mobile, $template, $code, int $expire = 1800): bool
    {
        $url = $this->api_url . 'tpl_single_send.json';
        $client = new HttpRequest;
        $postData = [
            'apikey'        => (string) config('sms_id'),
            'tpl_id'        => $template,
            'tpl_value'     => urlencode("#code#") . '=' . $code . "&" . urlencode("#expire#") . '=' . ($expire / 60),
            'mobile'        => (string) $mobile,
        ];
        $response = $client->headers([
            'Accept' => 'application/json',
            'Content-Type' => 'application/x-www-form-urlencoded;charset=utf-8'
        ])->post($url, $postData);
        $data = (string)$response->getBody();
        $json = json_decode($data, true);
        if (isset($json['code']) && $json['code'] == '0') {
            return true;
        } else {
            $this->setError($json['msg'] ?? '未知错误');
            return false;
        }
    }

    public function getError()
    {
        return $this->_error;
    }

    private function setError($error)
    {
        $this->_error = $error;
    }
}